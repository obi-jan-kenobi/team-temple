CREATE EXTENSION "uuid-ossp";

CREATE SCHEMA temple;

CREATE TABLE temple.team(
   id UUID PRIMARY KEY DEFAULT uuid_generate_v1mc(),
   name TEXT
);

CREATE TABLE temple.user(
   id UUID PRIMARY KEY DEFAULT uuid_generate_v1mc(),
   nickname TEXT,
   password TEXT
);

CREATE TABLE temple.team_user(
   team_id UUID FOREIGN KEY REFERENCES temple.team(id),
   user_id UUID FOREIGN KEY REFERENCES temple.user(id)
);
