{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE ScopedTypeVariables #-}

module Main where

import           System.Environment             ( getEnv )
import           Crypto.KDF.BCrypt              ( hashPassword
                                                , validatePassword
                                                )
import           Data.Aeson                     ( FromJSON
                                                , parseJSON
                                                , ToJSON
                                                , object
                                                , toJSON
                                                , withObject
                                                , (.:)
                                                , (.=)
                                                )
import           Web.Scotty
import           Web.Scotty.Trans               ( Parsable )
import           Control.Monad.Trans
import           Control.Monad                  ( liftM )
import           Network.HTTP.Types.Status      ( created201
                                                , notFound404
                                                )
import           Database.PostgreSQL.Simple
import           Database.PostgreSQL.Simple.FromRow
import           Database.PostgreSQL.Simple.ToRow
                                                ( toRow )
import           Database.PostgreSQL.Simple.ToField
                                                ( toField )
import           Database.PostgreSQL.Simple.Migration
import           Data.Monoid                    ( mconcat )
import           Data.Text.Lazy                 ( fromStrict
                                                , Text
                                                )
import qualified Data.Text.Lazy                as T
import qualified Data.ByteString.Char8         as B
import           System.Random                  ( randomIO )

import           Data.UUID                      ( UUID
                                                , toText
                                                , fromString
                                                )
import           Data.UUID.V4                   ( nextRandom )

instance Parsable UUID where
  parseParam = readEither

instance ToRow UUID where
  toRow id = [toField id]

insertTeam :: Query
insertTeam = "INSERT INTO temple.team (name) VALUES (?) RETURNING id"

getTeam :: Query
getTeam =
  "SELECT t.id, t.name, tu.user_id AS user FROM temple.team AS t JOIN temple.team_user AS tu ON t.id = tu.team_id WHERE t.id = ?"

insertUser :: Query
insertUser =
  "INSERT INTO temple.user (name, password) VALUES (?, ?) RETURNING id"

getUser :: Query
getUser = "SELECT u.id, u.name FROM temple.user WHERE u.id = ?"

data Config = Config
  { dbHost :: Text
  , dbUser :: Text
  , dbPassword :: Text
  , dbDatabase :: Text
  , dbPort :: Text
  , port :: Int
  }

data User = User
  { userID :: UUID
  , userNickname :: Text
  }

data NewUser = NewUser
  { newUserNickname :: Text
  , newUserPassword :: Text
  }

instance FromJSON NewUser where
  parseJSON =
    withObject "NewUser" $ \v -> NewUser <$> v .: "name" <*> v .: "password"

instance ToRow NewUser where
  toRow (NewUser name password) = [toField name, toField password]

data Team = Team
  { teamID :: UUID
  , teamName :: Text
  , teamUsers :: [UUID]
  }

asList Nothing  = []
asList (Just x) = [x]

instance ToJSON User where
  toJSON (User id name) = object ["id" .= id, "name" .= name]

instance FromRow User where
  fromRow = User <$> field <*> field

instance FromJSON Team where
  parseJSON =
    withObject "Team" $ \v -> Team <$> v .: "id" <*> v .: "name" <*> v .: "user"

instance ToJSON Team where
  toJSON (Team id name users) =
    object ["id" .= id, "name" .= name, "users" .= users]

instance FromRow Team where
  fromRow = Team <$> field <*> field <*> fmap (asList . fromString) field

data NewTeam = NewTeam
  { newTeamName :: Text
  } deriving (Show)

instance FromJSON NewTeam where
  parseJSON = withObject "NewTeam" $ \v -> NewTeam <$> v .: "name"

instance ToRow NewTeam where
  toRow (NewTeam name) = [toField name]

data Service = Service
  { serviceID :: UUID
  , serviceName :: Text
  , dependencies :: [Service]
  , owner :: Team
  }

instance FromJSON Service where
  parseJSON = withObject "Service" $ \v ->
    Service
      <$> v
      .:  "id"
      <*> v
      .:  "name"
      <*> v
      .:  "dependencies"
      <*> v
      .:  "owner"

instance ToJSON Service where
  toJSON (Service id name dependencies owner) = object
    [ "id" .= id
    , "name" .= name
    , "dependencies" .= dependencies
    , "owner" .= owner
    ]

main = do
  conn <- connectDb
  let dir = "./migrations"
  uuid <- nextRandom
  withTransaction conn $ runMigration $ MigrationContext
    MigrationInitialization
    True
    conn
  withTransaction conn $ runMigration $ MigrationContext
    (MigrationDirectory dir)
    True
    conn
  scotty 3000 $ do
    get "/users/" $ do
      uuid <- lift $ randomIO
      json [User uuid "testuser"]
    get "/users/:id" $ do
      id             <- (param "id") :: ActionM UUID
      user :: [User] <- lift $ query conn getUser id
      json user
    post "/users/" $ do
      (newUser :: NewUser)           <- jsonData
      hashedPassword :: B.ByteString <-
        lift $ hashPassword 12 $ B.pack $ T.unpack $ newUserPassword newUser
      [Only id] <-
        lift
        $ query conn insertUser
        $ NewUser (newUserNickname newUser)
        $ T.pack
        $ B.unpack hashedPassword
      status created201
      text $ fromStrict $ toText id
    get "/teams/:id" $ do
      id <- param "id" :: ActionM UUID
      xs <- lift $ query conn getTeam id
      case xs of
        [team :: Team] -> json team
        []             -> status notFound404
    post "/teams/" $ do
      newTeam :: NewTeam <- jsonData
      [Only id]          <- lift $ query conn insertTeam newTeam
      status created201
      text $ fromStrict $ toText id
    put "/teams/:id" $ do
      id <- param "id"
      json $ Service id "test" [] $ Team uuid "test" []
    delete "/teams/:id" $ do
      id <- param "id"
      json $ Service id "test" [] $ Team uuid "test" []


connectDb :: IO Connection
connectDb = do
  dbHost     <- getEnv "DATABASE_HOST"
  dbUser     <- getEnv "DATABASE_USER"
  dbPassword <- getEnv "DATABASE_PASSWORD"
  dbDatabase <- getEnv "DATABASE_DATABASE"
  connect defaultConnectInfo { connectHost     = dbHost
                             , connectUser     = dbUser
                             , connectPassword = dbPassword
                             , connectDatabase = dbDatabase
                             }
