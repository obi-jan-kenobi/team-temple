{-
Welcome to a Spago project!
You can edit this file as you like.
-}
{ name =
    "bacon-temple-ui"
, dependencies =
    [ "aff"
    , "arrays"
    , "console"
    , "effect"
    , "foldable-traversable"
    , "integers"
    , "react"
    , "react-dom"
    , "web-dom"
    , "web-events"
    , "web-html"
    , "web-socket"
    ]
, packages =
    ./packages.dhall
}
