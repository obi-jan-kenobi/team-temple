module Dashboard.Burndown where

import Graphics.D3

import Prelude (pure, bind, (+), ($), (-), (<<<), unit, map, show, (<>))
import Data.Maybe (Maybe(..))
import Data.Tuple (Tuple(..))

type Margin
  = {top :: Int, right :: Int, bottom :: Int, left :: Int}

createBurndown root width height sprintLength maxPoints = do
  let x_scale = scaleLinear (Tuple 0.0 (sprintLength - 1.0)) $ Tuple 0.0 width
  let y_scale = scaleLinear (Tuple 0.0 maxPoints) $ Tuple height 0.0
  let burndown_line = curve (curveMonotoneX $ lineY (\d -> y_scale d.value) $ lineX (\d -> x_scale d.day)) line
  svg <- map (attr "transform" (\_ -> "translate(" <> show 50.0 <> "," <> show 50.0 <> ")") <<< append "g" <<< attr "height" (\_ -> show $ height + 50.0 + 50.0) <<< attr "width" (\_ -> show $ width + 50.0 + 50.0)) $ select "svg" $ Just root
  -- let x = call (axisBottom x_scale) $ attr "transform" (\_ -> "translate(0," <> show 50.0 <> ")") $ append "g" svg
  -- let y = call (axisLeft y_scale) $ append "g" svg
  let path = attr "d" burndown_line $ attr "class" (\_ -> "line") $ bindDatum [{y: 20.0}, {y: 30.0}, {y: 12.0}] $ append "path" svg
  pure unit
