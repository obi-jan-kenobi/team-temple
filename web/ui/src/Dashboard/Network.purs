module Dashboard.Network where

import Graphics.D3

import Prelude hiding (append, join)

import Data.Array (length, zip, (..))
import Data.Tuple (fst, snd)
import Data.Int (toNumber)
import Data.Maybe (Maybe(..))
import Effect (Effect)
import Math (sqrt)

type Link
  = {source :: String, target :: String, value :: Int}

type Node
  = {group :: Int, id :: String}

type Team
  = {name :: String, group :: Int}

--createNetwork ::
--  {links :: Array Link, nodes :: Array Node, teams :: Array Team, root :: String} ->
--  Effect Unit
createNetwork { links, nodes, teams, root } = do
  r <- select root Nothing
  let simulation = force "center" (forceCenter (640.0 / 2.0) (480.0 / 2.0)) $ force "charge" forceManyBody $ force "link" (forceLink (\d -> d.id) links) $ forceSimulation nodes
  svg <- map (attr "stroke-opacity" (const (show 0.6)) <<< attr "stroke" (const "#999") <<< append "g") (select "svg" (Just r))
  link_lines <- map (attr "stroke-width" (\d -> show $ sqrt $ toNumber d.value) <<< join "line" <<< bindData links) (selectAll "line" (Just svg))
  let node_node = append "g" svg
  let node_stroke = attr "stroke" (const "#fff") node_node
  let node_stroke_width = attr "stroke-width" (const (show 1.5)) node_stroke
  node_circles <- selectAll "circle" (Just (node_stroke_width :: Selection Unbound))
  let node_bound = bindData nodes node_circles
  let node_joined = join "circle" node_bound
  let node_r = attr "r" (const (show 5)) node_joined
  let node_fill = attr "fill" (color (\d ->
        d.group)) node_r
  let node_call = call (drag simulation) node_fill
  let node_title = append "title" node_call
  let node_text = text (\d ->
        d.id) node_title
  let simulation_on = on "tick" link_lines node_call simulation
  let legend_node = append "g" svg
  let legend_font_size = attr "font-size" (const (show 10)) legend_node
  let legend_text_anchor = attr "text-anchor" (const "end") legend_font_size
  legend_g <- selectAll "g" (Just legend_text_anchor)
  let legend_bound = bindData (zip (1 .. length teams) teams) legend_g
  let legend_enter = enter legend_bound
  let legend_append = append "g" legend_enter
  let l_x = attr "x" (const $ show 460) legend_append
  let l_width = attr "width" (const $ show 30) l_x
  let l_height = attr "height" (const $ show 15) l_width
  let l_fill = attr "fill" (color (\d ->
        (snd d).group)) l_height
  let l_stroke = attr "stroke" (color (\d ->
        (snd d).group)) l_fill
  let l_stroke_width = attr "stroke-width" (const $ show 0.5) l_stroke
  let l_transform = attr "transform" (\d ->
        "translate(0," <> show ((fst d) * 20) <> ")") l_stroke_width
  let legend_rect = append "rect" l_stroke_width
  let l_rect_x = attr "x" (const $ show 460) legend_rect
  let l_rect_width = attr "width" (const $ show 15) l_rect_x
  let l_rect_height = attr "height" (const $ show 15) l_rect_width
  let l_rect_fill = attr "fill" (color (\d ->
        (snd d).group)) l_rect_height
  let l_text = append "text" l_transform
  let l_text_x = attr "x" (const $ show 560) l_text
  let l_text_y = attr "y" (const $ show 11) l_text_x
  let l_text_dy = attr "dy" (const "0.32rem") l_text_y
  let l_text_text = text (\d ->
        (snd d).name) l_text_dy
  pure unit
