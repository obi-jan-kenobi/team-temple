const d3 = require("d3");

exports._selectImpl = function(select) {
  return function(root) {
    return function() {
      return root.value0 ? root.value0.select(select) : d3.select(select);
    };
  };
};

exports._selectAllImpl = function(select) {
  return function(root) {
    return function() {
      return root.value0 ? root.value0.selectAll(select) : d3.selectAll(select);
    };
  };
};

exports._dataImpl = function(data) {
  return function(selection) {
    return selection.data(data);
  };
};

exports._datumImpl = function(data) {
  return function(selection) {
    return selection.datum(data);
  };
};

exports._enterImpl = function(update) {
  return update.enter();
};

exports._appendImpl = function(element) {
  return function(selection) {
    return selection.append(element);
  };
};

exports._styleImpl = function(attr) {
  return function(callback) {
    return function(selection) {
      return selection.style(attr, callback);
    };
  };
};

exports._textImpl = function(callback) {
  return function(selection) {
    return selection.text(callback);
  };
};

exports._attrImpl = function(attr) {
  return function(callback) {
    return function(selection) {
      return selection.attr(attr, callback);
    };
  };
};

exports._joinImpl = function(string) {
  return function(selection) {
    return selection.join(string);
  };
};

exports._scaleOrdinalImpl = function(domain) {
  return function(range) {
    return d3.scaleOrdinal(domain, range);
  };
};

exports._scaleLinearImpl = function(domain) {
  return function(range) {
    return d3.scaleLinear(domain, range);
  };
};

exports._forceSimulationImpl = function(nodes) {
  return d3.forceSimulation(nodes);
};

d3.forceSimulation([]).force;

exports._forceImpl = function(attr) {
  return function(callback) {
    return function(simulation) {
      return simulation.force(attr, callback);
    };
  };
};

exports._forceLinkImpl = function(id_callback) {
  return function(links) {
    return d3.forceLink(links).id(id_callback);
  };
};

exports._forceManyBodyImpl = d3.forceManyBody();

exports._forceCenterImpl = function(width) {
  return function(height) {
    return d3.forceCenter(width, height);
  };
};

exports._dragImpl = function(simulation) {
  function dragstarted(d) {
    if (!d3.event.active) simulation.alphaTarget(0.3).restart();
    d.fx = d.x;
    d.fy = d.y;
  }

  function dragged(d) {
    d.fx = d3.event.x;
    d.fy = d3.event.y;
  }

  function dragended(d) {
    if (!d3.event.active) simulation.alphaTarget(0);
    d.fx = null;
    d.fy = null;
  }

  return d3
    .drag()
    .on("start", dragstarted)
    .on("drag", dragged)
    .on("end", dragended);
};

exports._onImpl = function(event) {
  return function(link) {
    return function(node) {
      return function(simulation) {
        simulation.on(event, function() {
          link
            .attr("x1", function(d) {
              return d.source.x;
            })
            .attr("y1", function(d) {
              return d.source.y;
            })
            .attr("x2", function(d) {
              return d.target.x;
            })
            .attr("y2", function(d) {
              return d.target.y;
            });

          node
            .attr("cx", function(d) {
              return d.x;
            })
            .attr("cy", function(d) {
              return d.y;
            });
        });
      };
    };
  };
};

exports._callImpl = function(fn) {
  return function(selection) {
    return selection.call(fn);
  };
};

exports._colorImpl = function(map) {
  return function(d) {
    const scale = d3
      .scaleSequential()
      .domain([0, 5])
      .interpolator(d3.interpolateRainbow);
    const scaled = scale(map(d));
    return scaled;
  };
};

exports._lineImpl = d3.line;

exports._lineXImpl = function(fn) {
  return function(line) {
    return line.x(fn);
  };
};

exports._lineYImpl = function(fn) {
  return function(line) {
    return line.y(fn);
  };
};

exports._curveImpl = function(fn) {
  return function(line) {
    return line.curve(fn);
  };
};

exports._axisBottomImpl = function(scale) {
  return d3.axisBottom(scale);
};

exports._axisLeftImpl = function(scale) {
  return d3.axisLeft(scale);
};

exports._curveMonotoneXImpl = d3.curveMonotoneX
