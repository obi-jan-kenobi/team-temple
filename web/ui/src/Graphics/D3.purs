module Graphics.D3
  ( Force
  , Line
  , Selection
  , Simulation
  , Unbound
  , append
  , attr
  , axisBottom
  , axisLeft
  , bindData
  , bindDatum
  , call
  , color
  , curve
  , curveMonotoneX
  , drag
  , enter
  , force
  , forceCenter
  , forceLink
  , forceManyBody
  , forceSimulation
  , join
  , line
  , lineX
  , lineY
  , on
  , scaleLinear
  , scaleOrdinal
  , select
  , selectAll
  , style
  , text
  ) where

import Data.Tuple

import Data.Maybe (Maybe)
import Effect (Effect)
import Prelude (Unit)

foreign import data Selection :: Type -> Type

foreign import data Simulation :: Type -> Type

foreign import data Force :: Type

foreign import data Line :: Type -> Type

foreign import data Scale :: Type -> Type -> Type

data Unbound

foreign import _selectImpl :: forall a. String -> Maybe (Selection a) -> Effect (Selection a)

select :: forall a. String -> Maybe (Selection a) -> Effect (Selection a)
select = _selectImpl

foreign import _selectAllImpl :: forall a. String -> Maybe (Selection a) -> Effect (Selection a)

selectAll :: forall a. String -> Maybe (Selection a) -> Effect (Selection a)
selectAll = _selectAllImpl

foreign import _dataImpl :: forall a b. Array a -> Selection b -> Selection a

bindData :: forall a b. Array a -> Selection b -> Selection a
bindData = _dataImpl

foreign import _datumImpl :: forall a b. Array a -> Selection b -> Selection a

bindDatum = _datumImpl

foreign import _enterImpl :: forall a. Selection a -> Selection a

enter :: forall a. Selection a -> Selection a
enter = _enterImpl

foreign import _appendImpl :: forall a. String -> Selection a -> Selection a

append :: forall a. String -> Selection a -> Selection a
append = _appendImpl

foreign import _styleImpl :: forall a. String -> (a -> String) -> Selection a -> Selection a

style :: forall a. String -> (a -> String) -> Selection a -> Selection a
style = _styleImpl

foreign import _textImpl :: forall a. (a -> String) -> Selection a -> Selection a

text :: forall a. (a -> String) -> Selection a -> Selection a
text = _textImpl

foreign import _joinImpl :: forall a. String -> Selection a -> Selection a

join :: forall a. String -> Selection a -> Selection a
join = _joinImpl

foreign import _scaleOrdinalImpl :: forall a b. Array a -> Array b -> (a -> b)

scaleOrdinal :: forall a b. Array a -> Array b -> (a -> b)
scaleOrdinal = _scaleOrdinalImpl

foreign import _scaleLinearImpl :: forall a b. Array a -> Array b -> (a -> b)

scaleLinear :: forall a b. Tuple a a -> Tuple b b -> (a -> b)
scaleLinear domain range = _scaleLinearImpl [fst domain, snd domain] [ fst range
                                                                     , snd range
                                                                     ]

foreign import _attrImpl :: forall a. String -> (a -> String) -> Selection a -> Selection a

attr :: forall a. String -> (a -> String) -> Selection a -> Selection a
attr = _attrImpl

foreign import _colorImpl :: forall a b c. (a -> c) -> (a -> b)

color :: forall a b c. (a -> c) -> (a -> b)
color = _colorImpl

foreign import _forceSimulationImpl :: forall a. Array a -> Simulation a

forceSimulation :: forall a. Array a -> Simulation a
forceSimulation = _forceSimulationImpl

foreign import _forceImpl :: forall a. String -> Force -> Simulation a -> Simulation a

force :: forall a. String -> Force -> Simulation a -> Simulation a
force = _forceImpl

foreign import _forceLinkImpl :: forall a b. (a -> String) -> Array b -> Force

forceLink :: forall a b. (a -> String) -> Array b -> Force
forceLink = _forceLinkImpl

foreign import _forceManyBodyImpl :: Force

forceManyBody :: Force
forceManyBody = _forceManyBodyImpl

foreign import _forceCenterImpl :: Number -> Number -> Force

forceCenter :: Number -> Number -> Force
forceCenter = _forceCenterImpl

foreign import _onImpl :: forall a b c. String -> Selection a -> Selection b -> Simulation c -> Unit

on :: forall a b c. String -> Selection a -> Selection b -> Simulation c -> Unit
on = _onImpl

foreign import _dragImpl :: forall a b. Simulation a -> (b -> Unit)

drag :: forall a b. Simulation a -> (b -> Unit)
drag = _dragImpl

foreign import _callImpl :: forall a b. (b -> Unit) -> Selection a -> Selection a

call :: forall a b. (b -> Unit) -> Selection a -> Selection a
call = _callImpl

foreign import _lineImpl :: forall a. Line a

line :: forall a. Line a
line = _lineImpl

foreign import _lineXImpl :: forall a. (a -> Number) -> Line a -> Line a

lineX :: forall a. (a -> Number) -> Line a -> Line a
lineX = _lineXImpl

foreign import _lineYImpl :: forall a. (a -> Number) -> Line a -> Line a

lineY :: forall a. (a -> Number) -> Line a -> Line a
lineY = _lineYImpl

foreign import _axisBottomImpl :: forall a b. (a -> b) 
axisBottom :: forall a b. (a -> b) 
axisBottom = _axisBottomImpl

foreign import _axisLeftImpl :: forall a b. (a -> b) 

axisLeft :: forall a b. (a -> b) 
axisLeft = _axisLeftImpl

foreign import _curveImpl :: forall a b. a -> Line b -> Line b

curve :: forall a b. a -> Line b -> Line b
curve = _curveImpl

foreign import _curveMonotoneXImpl :: forall a b c. a -> b -> c

curveMonotoneX :: forall a b c. a -> b -> c
curveMonotoneX = _curveMonotoneXImpl
