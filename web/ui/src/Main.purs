module Main where

import Dashboard.Burndown

import Dashboard.Network (Link, Node, Team, createNetwork)
import Data.Maybe (Maybe(..))
import Effect (Effect)
import Graphics.D3 (select)
import Web.Socket.WebSocket
import Web.Socket.Event.MessageEvent
import Web.Event.EventTarget
import Prelude (Unit, bind, discard, pure, unit, map, ($))
import Web.HTML.HTMLDocument (toNonElementParentNode) as DOM
import Web.DOM.NonElementParentNode (getElementById) as DOM
import Web.HTML (window) as DOM
import Web.HTML.Window (document) as DOM

const :: forall t3 t4. t3 -> t4 -> t3
const = \x y ->
  x

--createSocket url cb = do
--  et <- map toEventTarget $ create "ws://api/prs" []
--  eventListener \event -> (fromEvent event) \msg -> cb

main :: Effect Unit
main = do
  window <- DOM.window
  document <- DOM.document window
  let
      node = DOM.toNonElementParentNode document
  burndown_root <- DOM.getElementById "burndown-chart" node
  createNetwork { links: mock_links, nodes: mock_nodes, teams: teams, root:".network-graph"}
  _ <- createBurndown burndown_root 640.0 480.0 10.0 40.0
  pure unit


mock_nodes :: Array Node
mock_nodes = [ { id: "Reservation", group: 1 }
             , { id: "SAP PI", group: 5 }
             , { id: "Storelocator", group: 2 }
             , { id: "Productviews", group: 3 }
             , { id: "GraphQL", group: 3 }
             , { id: "MailService", group: 2 }
             , { id: "Frontend", group: 4 }
             , { id: "SPA", group: 4 }
             ]


mock_links :: Array Link
mock_links = [ { source: "Reservation", target: "SAP PI", value: 1 }
             , { source: "Reservation", target: "MailService", value: 10 }
             , { source: "Reservation", target: "Productviews", value: 6 }
             , { source: "Reservation", target: "Storelocator", value: 10 }
             , { source: "SAP PI", target: "Reservation", value: 10 }
             , { source: "GraphQL", target: "Reservation", value: 10 }
             , { source: "Frontend", target: "GraphQL", value: 10 }
             , { source: "SPA", target: "GraphQL", value: 10 }
             ]

teams :: Array Team
teams = [ { name: "Bacon", group: 1 }
        , { name: "Sibylle", group: 2 }
        , { name: "Roadrunner", group: 3 }
        , { name: "Mixed", group: 4 }
        , { name: "Extern", group: 5 }
        ]


